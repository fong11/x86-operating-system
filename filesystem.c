#include "syscalls.h"
#include "filesystem.h"
#include "lib.h"
#include "multiboot.h"


uint32_t index_offset;  
uint32_t data_offset; 
uint8_t fname[1024];
/*
 * file_open
 *	 DESCRIPTION:  Sets the global fname to the specificed filename
	 INPUTS: const uint8_t* filename
 *   OUTPUTS: none
 *   RETURN VALUE: 0 on success
 *   SIDE EFFECTS: sets the global fname to the correct filename
 */
uint32_t file_open(const uint8_t* filename){
	data_offset =  0; 
	int i;
	for(i=0;i<1024;i++){
		fname[i] = '\0';
	}
	strncpy((int8_t*)fname, (int8_t*)filename, strlen((int8_t*)filename));
	return 0;
}

/*
 * file_read
 *	 DESCRIPTION: this function uses read_dentry_by_name and read_data
					in order to find the name of the file and then
					read its respective data
	 INPUTS: int32_t fd, void* buf, int32_t length
 *   OUTPUTS: buf - the data within the file of size length
 *   RETURN VALUE: -1 on failure, number of bytes read on success,
					0 if already at end of file
 *   SIDE EFFECTS: reads the file and returns the file data
 */

uint32_t file_read(int32_t fd, void* buf, int32_t length){
	int blah;
	
	pcb_t* current_process;
	current_process = get_process();
	
	int file_pos = current_process->file_array[fd].file_pos;
	uint32_t inode = current_process->file_array[fd].inode;
	blah = read_data(inode,file_pos, buf, length);
	return blah;
	
	return -1;
}
/*
 * read_dentry_by_name
 *	 DESCRIPTION: Uses the fname in order to find the correct dentry within
				the bootblock
	 INPUTS: int32_t fd, void* buf, int32_t length
 *   OUTPUTS: dentry - the dentry corresponding to the fname
 *   RETURN VALUE: -1 on failure
					0 on success
 *   SIDE EFFECTS: uses the fname to find the correct dentry
 */
int32_t read_dentry_by_name(const uint8_t* fname, dentry_t* dentry){

	int i = 0;
	int j = 0;
	
	for(i = 0; i < bootblock_addr->num_entries; i++){
		/*Find the file name given in the directory*/
		if(strncmp((int8_t*)fname, (int8_t *)bootblock_addr->entries[i].filename, FILENAME_SIZE) == 0){ 
			
			/*Copying name*/
			for(j = 0; j < FILENAME_SIZE; j++){
				dentry->filename[j] = bootblock_addr->entries[i].filename[j];
			}
			
			/*Copying type*/
			dentry->filetype = bootblock_addr->entries[i].filetype;
			/*Copying inode*/
			dentry->inode = bootblock_addr->entries[i].inode;
			
			/*Copying reserved data*/
			for(j = 0; j < 24; j++){
				dentry->reserved[j] = bootblock_addr->entries[i].reserved[j];
			}
			return 0;
		}	
	}	
	return -1;

}
/*
 * read_dentry_by_index
 *	 DESCRIPTION: Uses the index in order to find the correct dentry within
				the bootblock
	 INPUTS: uint32_t index, dentry_t* dentry
 *   OUTPUTS: dentry - the dentry corresponding to the index
 *   RETURN VALUE: -1 on failure
					0 on success
 *   SIDE EFFECTS: uses the index to find the correct dentry
 */
int32_t read_dentry_by_index(uint32_t index, dentry_t* dentry){
	
	bootblock_t bblock;
	bblock = *bootblock_addr;
	
	dentry_t dir_entry = bblock.entries[index];
	
	int j; 
	
	/*Copying name*/
	for(j=0;j<FILENAME_SIZE;j++){
		dentry->filename[j] = dir_entry.filename[j];
	}
	
	/*Copying type*/
	dentry->filetype = dir_entry.filetype;
	/*Copying inode*/
	dentry->inode = dir_entry.inode;
			
	/*Copying reserved data*/
	for(j = 0;j < 24;j++){
		dentry->reserved[j] = dir_entry.reserved[j];
	}
	return 0; //successful

}
/*
 * read_data
 *	 DESCRIPTION: Uses the inode to find the entry in the bootblock and extract
				the file data
	 INPUTS: uint32_t inode, uint32_t offset, uint8_t* buf, uint32_t length
 *   OUTPUTS: buf - the data that it read from the file of size length
 *   RETURN VALUE: -1 on failure, number of bytes read on success,
					0 if already at end of file
 *   SIDE EFFECTS: uses the inode number to read from the file based on a previous offset
 */
int32_t read_data(uint32_t inode, uint32_t offset, uint8_t* buf, uint32_t length){

    uint32_t* node;
    uint32_t* data;
    int i, total = 0; 
    uint32_t read_len = 0;
	data_offset = offset;

	
    /*Search for the directory entry with the same inode*/
        for(i=1;i<bootblock_addr->num_entries;i++){
        
            if((bootblock_addr->entries[i]).inode == inode){
				
                /*Go to the corresponding inode*/
                node = (uint32_t*)((uint32_t)bootblock_addr + FOUR_KB*(inode+1));

                /*Get pointer to first data block we need to read*/
                    data = (node + (data_offset/FOUR_KB + 1));
                    
                    /*calculate the number of data blocks the file */
					
					/*find offset within the first data block being read*/
					//start_datablock = data_offset%FOUR_KB;
					
					
					
                    //num_dblocks = (length +data_offset)/FOUR_KB +1;
                    
					/*If the offset does not exceed the file length continue*/ 
					if(data_offset < *node){
					
                    /*for each datablock*/
                    
                   // while(length > 0){
						if(length > (*node-data_offset)){
							length = *node - data_offset;
						}
				   /*Check if length is within offset and end of data block*/
						if(length <= (FOUR_KB - data_offset%FOUR_KB)){
						/*Read the data if it is*/
							memcpy((void*)buf, (void*)((uint32_t)bootblock_addr + FOUR_KB*(1 + bootblock_addr->num_inodes + *data) + data_offset%FOUR_KB), length);
							data_offset +=length;
							return length;
						}else{
						
						/*Length exceeds the bound of datablock so read the data*/
							memcpy((void*)buf, (void*)((uint32_t)bootblock_addr + FOUR_KB*(1 + bootblock_addr->num_inodes + *data) + data_offset%FOUR_KB), FOUR_KB - data_offset%FOUR_KB);
                        //data_offset += read;
						
							length -= (FOUR_KB - data_offset%FOUR_KB);
							total += (FOUR_KB - data_offset%FOUR_KB);
							read_len=FOUR_KB- (data_offset%FOUR_KB);
							data_offset +=  (FOUR_KB - data_offset%FOUR_KB);
							data++;
							
						}
					
					/*We are now at the start of a datablock, therefore we can read 4KB chunks*/
						while(length >= FOUR_KB){
					
							memcpy((void*)buf + read_len,(void*)((uint32_t)bootblock_addr + FOUR_KB*(1 + bootblock_addr->num_inodes + *data)), FOUR_KB);
                        
							data_offset += FOUR_KB;
							length -= FOUR_KB;
							total += FOUR_KB;
							data++;
							read_len+=FOUR_KB;
						}
					
					
					/*read the last section that is less than 4 KB of the last datablock*/
						if(length > 0){
							memcpy((void*)buf + read_len, (void*)((uint32_t)bootblock_addr + FOUR_KB*(1 + bootblock_addr->num_inodes + *data)), length);
						
							data_offset += length;
							total += length;
						}

						if(data_offset <= *node){
							return total;
						}else{
							return 0;
						}
					
					}else{
						return 0;
					}	
			}

        }       
    
    return -1;
    

}
/*
 * file_write
 *	 DESCRIPTION: returns -1 because this is a read-only filesystem
	 INPUTS: uint8_t* buf, uint32_t length
 *   OUTPUTS: none
 *   RETURN VALUE: -1 on failure
 *   SIDE EFFECTS: none
 */
uint32_t file_write(uint8_t* buf, uint32_t length){
	return -1;
}
/*
 * file_close
 *	 DESCRIPTION: closes the file, not necessary to clear the fname 
	 INPUTS: none
 *   OUTPUTS: none
 *   RETURN VALUE: 0 on success
 *   SIDE EFFECTS: none
 */
uint32_t file_close(){
	return 0;
}
/*
 * dir_open
 *	 DESCRIPTION: resets index_offset
	 INPUTS: none
 *   OUTPUTS: none
 *   RETURN VALUE: 0 on success
 *   SIDE EFFECTS: none
 */
uint32_t dir_open(){
	index_offset = 0;
	return 0;
}
/*
 * dir_read
 *	 DESCRIPTION:It returns all the directory information in a buffer from the boot block.
				of a particular directory.
	 INPUTS: int32_t fd, int8_t* buf, int32_t length
 *   OUTPUTS: buf - filename of the next file in the directory
 *   RETURN VALUE: 0 on success
 *   SIDE EFFECTS: moves to the next filename in the directory to print out each file
 */
uint32_t dir_read(int32_t fd, int8_t* buf, int32_t length){
	dentry_t* cur_dentry;
	int num_entries = bootblock_addr->num_entries;
	int i;
	for(i = 0; i < num_entries; i++){
		cur_dentry = &(bootblock_addr->entries[i]);
		
		if(i >= index_offset){
			strncpy(buf, cur_dentry->filename, FILENAME_SIZE); 
			index_offset++; //move to next directory entry
			return strlen(cur_dentry->filename);
		}
	}
	return 0;
}
/*
 * dir_write
 *	 DESCRIPTION: returns -1, read only filesystem
	 INPUTS: uint8_t* buf, uint32_t length
 *   OUTPUTS: none
 *   RETURN VALUE: -1 on on failure
 *   SIDE EFFECTS: none
 */
uint32_t dir_write(uint8_t* buf, uint32_t length){
	return -1;
}
/*
 * dir_close
 *	 DESCRIPTION: closes the directory
	 INPUTS: none
 *   OUTPUTS: none
 *   RETURN VALUE: 0 on success
 *   SIDE EFFECTS: none
 */
uint32_t dir_close(){
	return 0;
}
