#include "paging.h"
#include "types.h"

/*Page tables and page directories used by processes aligned with 4KB*/

uint32_t __attribute__((aligned(FOUR_KB))) page_directory[NUM_ENTRIES];
uint32_t __attribute__((aligned(FOUR_KB))) page_table_1[NUM_ENTRIES];

uint32_t __attribute__((aligned(FOUR_KB))) page_directory_p1[NUM_ENTRIES];
uint32_t __attribute__((aligned(FOUR_KB))) page_table_p1[NUM_ENTRIES];

uint32_t __attribute__((aligned(FOUR_KB))) page_directory_p2[NUM_ENTRIES];
uint32_t __attribute__((aligned(FOUR_KB))) page_table_p2[NUM_ENTRIES];

uint32_t __attribute__((aligned(FOUR_KB))) page_directory_p3[NUM_ENTRIES];
uint32_t __attribute__((aligned(FOUR_KB))) page_table_p3[NUM_ENTRIES];

uint32_t __attribute__((aligned(FOUR_KB))) page_directory_p4[NUM_ENTRIES];
uint32_t __attribute__((aligned(FOUR_KB))) page_table_p4[NUM_ENTRIES];

uint32_t __attribute__((aligned(FOUR_KB))) page_directory_p5[NUM_ENTRIES];
uint32_t __attribute__((aligned(FOUR_KB))) page_table_p5[NUM_ENTRIES];

uint32_t __attribute__((aligned(FOUR_KB))) page_directory_p6[NUM_ENTRIES];
uint32_t __attribute__((aligned(FOUR_KB))) page_table_p6[NUM_ENTRIES];

/*
* initialize_paging
 *	 DESCRIPTION: initialises the paging
 *   INPUTS: none
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *   SIDE EFFECTS: creates a page directory and one page tables that maps kernel and video mem.
 */

void initialize_paging(){
	
	int i;
	
	/*initialise each entry to R/W and not present*/
	for(i = 0; i<NUM_ENTRIES; i++){
		
		page_directory[i] = ONLY_RW;
		//page_directory[i] = 0;
		//page_directory[i] |= 2;
	}

	uint32_t address;
	
	/*Make the entries for page table for 0MB-4MB*/
	
	/*Set address 0 to not present */
	address = 0;
	
	/*Set the rest of the pages to present and accessable by all*/
	for(i = 0; i<NUM_ENTRIES; i++){
		page_table_1[i] = address | SET_PRESENT_SUP_RW;
		address += FOUR_KB;
	}
	
	/*Make sure NULL cannot be dereferenced by setting it to not present*/
	page_table_1[0] = ONLY_RW;
	
	/*place the page table in the first entry of directory*/
	page_directory[0] = (uint32_t)(page_table_1) | SET_PRESENT_SUP_RW; 
	
	/*set the first page in page table 2 to 4MB (kernel)
	make size of each page to 4MB
	set the page as present and R/W*/

	page_directory[1] = (uint32_t)( KERNEL_ADDRESS | SET_4MB_PAGES |  SET_PRESENT_RW);
	

	/*Make the cr3 reg to point to the page directory*/
	asm volatile ("movl %0, %%cr3 "
					:
					:"b" (page_directory)
					);

	/*Enable 4MB pages by enabling bit 5 of reg cr4*/
    uint32_t cr4;
    asm volatile("movl %%cr4, %0"
					:"=b"(cr4)
					:
					);

    cr4 |= ENABLE_4MB_PAGES;

    asm volatile("movl %0, %%cr4"
    				:
					:"b"(cr4)
					);
	
	/*Enable paging by setting the most sig bit of reg cr0*/
    uint32_t cr0;
    asm volatile("movl %%cr0, %0"
    				:"=b"(cr0)
					:
					);
    cr0 |= ENABLE_PAGING;

    asm volatile("movl %0, %%cr0"
    				:
					:"b"(cr0)
					);
	
}

/*
* map_virtual_mem
 *	 DESCRIPTION: maps the virtual memory to a specified format given a physical address
 *   INPUTS: Physical address
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *   SIDE EFFECTS: creates a page directory and one page table that maps kernel and video mem 
 *				   Maps process to 128MB
 */

void map_virtual_mem(uint32_t phys_addr){

	int i;
	uint32_t *pd, *pt1;
	
	//Use one of the 6 page tables for the 6 processes
	
	if(phys_addr == EIGHT_MB){							//Process 1
		pd = page_directory_p1;
		pt1 = page_table_p1;
	}else if(phys_addr == EIGHT_MB + FOUR_MB*1){		//Process 2
		pd = page_directory_p2;
		pt1 = page_table_p2;
	}else if(phys_addr == EIGHT_MB + FOUR_MB*2){		//Process 3
		pd = page_directory_p3;
		pt1 = page_table_p3;
	}else if(phys_addr == EIGHT_MB + FOUR_MB*3){		//Process 4
		pd = page_directory_p4;
		pt1 = page_table_p4;
	}else if(phys_addr == EIGHT_MB + FOUR_MB*4){		//Process 5
		pd = page_directory_p5;
		pt1 = page_table_p5;
	}else if(phys_addr == EIGHT_MB + FOUR_MB*5){		//Process 6
		pd = page_directory_p6;
		pt1 = page_table_p6;
	}


	/*initialise each entry to R/W and not present*/
	for(i = 0; i<NUM_ENTRIES; i++){
		
		pd[i] = ONLY_RW;

	}


	/*set the first page in page table 2 to 4MB (kernel)
	make size of each page to 4MB
	set the page as present and R/W*/

	pd[1] = KERNEL_ADDRESS | SET_4MB_PAGES |  SET_PRESENT_RW;

	/*set 4MB page in the page directory
	place it in virtual memory 128MB*/

	pd[32] = phys_addr | SET_4MB_PAGES | SET_PRESENT_SUP_RW;


	/*Time to map video memory*/
	uint32_t address = 0;
	
	/*Set the rest of the pages to present and accessable by all*/
	for(i = 0; i<NUM_ENTRIES; i++){
		pt1[i] = address | SET_PRESENT_SUP_RW;
		address += FOUR_KB;
	}
	
	/*Make sure NULL cannot be dereferenced by setting it to not present*/
	pt1[0] = ONLY_RW;

	pd[0] = (uint32_t)(pt1) | SET_PRESENT_SUP_RW; 





	/*Make the cr3 reg to point to the page directory*/
	asm volatile ("movl %0, %%cr3 "
					:
					:"b" (pd)
					);

	/*Enable 4MB pages by enabling bit 5 of reg cr4*/
    uint32_t cr4;
    asm volatile("movl %%cr4, %0"
					:"=b"(cr4)
					:
					);

    cr4 |= ENABLE_4MB_PAGES;

    asm volatile("movl %0, %%cr4"
    				:
					:"b"(cr4)
					);
	
	/*Enable paging by setting the most sig bit of reg cr0*/
    uint32_t cr0;
    asm volatile("movl %%cr0, %0"
    				:"=b"(cr0)
					:
					);
    cr0 |= ENABLE_PAGING;

    asm volatile("movl %0, %%cr0"
    				:
					:"b"(cr0)
					);





}


/*
* map_vid_mem
 *	 DESCRIPTION: Used to remap the video memory so terminals can write to their respective buffer
 *   INPUTS: Address of buffer
 *   OUTPUTS:
 *   RETURN VALUE: none
 *   SIDE EFFECTS: Remaps the virtual address of video memory to another address
 *				  
 */

void map_vid_mem(uint32_t* addr){
	
	uint32_t *pd, *pt1;
	
	/*Get the current page directory*/
	asm volatile ("movl %%cr3, %0"
					:"=b" (pd)
					:
					);
	
	//Get the first entry of pd 
	pt1 = (uint32_t*) pd[0];
	
	//Remove the first 12 bits
	pt1 = (uint32_t*)((uint32_t)pt1 & 0xFFFFF000);
	
	//map address to the page table entry where video memory is 0xB8000
	pt1[184] = (uint32_t)(addr)| SET_PRESENT_SUP_RW;
	
	//Load the page directory to cr3
	asm volatile ("movl %0, %%cr3"
					:
					:"b" (pd)
					);
	
}

