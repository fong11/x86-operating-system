#ifndef _FILESYSTEM_H
#define _FILESYSTEM_H

#include "lib.h"
#include "multiboot.h"

#define FILENAME_SIZE 32
#define NUM_DIRECTORIES 64
#define FOUR_KB 4096

//dentry created by looking at appendix a
//filetype and inode are both 4B (int32)
typedef struct dentry{
		int8_t filename[32];
		uint32_t filetype;
		uint32_t inode;
		uint8_t reserved[24];
} dentry_t;


//4KB total, first byte is the first four things below
typedef struct bootblock{
		uint32_t num_entries;
		uint32_t num_inodes;
		uint32_t num_datablocks;
		uint8_t reserved[52];
		dentry_t entries[63]; // 63 of 64B directory entries
} bootblock_t;


 bootblock_t* bootblock_addr;
extern uint32_t file_open(const uint8_t* filename);
extern uint32_t file_read(int32_t fd, void* buf, int32_t length);
extern int32_t read_dentry_by_name(const uint8_t* fname, dentry_t* dentry);
extern int32_t read_dentry_by_index(uint32_t index, dentry_t* dentry);
extern int32_t read_data(uint32_t inode, uint32_t offset, uint8_t* buf, uint32_t length);
extern uint32_t file_write(uint8_t* buf, uint32_t length);
extern uint32_t file_close();
extern uint32_t dir_open();
extern uint32_t dir_read();
extern uint32_t dir_write(uint8_t* buf, uint32_t length);
extern uint32_t dir_close();

#endif
