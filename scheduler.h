#ifndef _SCHEDULER_H
#define _SCHEDULER_H

#include "lib.h"
#include "multiboot.h"
#include "types.h"
#include "x86_desc.h"
#include "rtc.h"
#include "i8259.h"
#include "filesystem.h"
#include "paging.h"
#include "syscalls.h"

extern void pit_handler();
void scheduler();

#endif
