#include "syscalls.h"
#include "lib.h"
#include "multiboot.h"
#include "types.h"
#include "keyboard.h"
#include "x86_desc.h"
#include "rtc.h"
#include "i8259.h"
#include "filesystem.h"
#include "paging.h"

#define IMG_SIZE 0x00400000
#define VIRTUAL_ADDR 0x08000000

int32_t arg_id, arg_len;
int ret_addr;

//pcb_t processes[MAX_NUM_OF_PROCESSES];
/*Information about the number of processes*/
int num_processes = 0;						//Number of processes
int process_array[] = {0, 0 , 0, 0, 0, 0};	//process array shows which process (by index) is running, 1 if process is running
int term_process_info[] = {0, 0, 0};		//How many processes are running and in which terminal
//terminates a process

/*
* halt
 *	 DESCRIPTION: ends a user process
 *   INPUTS: the status of the process
 *   OUTPUTS:
 *   RETURN VALUE: 
 *   SIDE EFFECTS: Remaps the virtual address of video memory to another address
 *				  
 */
int32_t halt(uint8_t status){
	/* returns 8-bit argument from BL into a 32-bit return value!!
	do not return all of EBX*/
	
	pcb_t *curr_proc;
	
	curr_proc = get_process();				//Get the current process running
	int term =  curr_proc->parent_term;		//Get which terminal the process runs in
	int proc_num = curr_proc->proc_num;		//Get the process id
	
	//If process to be halted is root shell, re-execute shell
	if(term_process_info[term] == 1){
		terminal_write(2,"Exited from Root Shell\nRebooting Shell\n",39); 
		term_process_info[term] = 0;		//No processes running in terminal
		num_processes--;
		execute((uint8_t*)"shell");
	}
	
	term_process_info[term]--;		//Decrement the number of processes
	process_array[proc_num] = 0;	//Set that process to not running
	num_processes--;
	
	if(curr_proc->parent_pcb != NULL){
		
		curr_proc = (pcb_t*)curr_proc->parent_pcb;		//get the parent process
		curr_proc->parent_term = term;					//get the terminal it's running in
		proc_num = curr_proc->proc_num;					//Get the process id
		terminal_screens[term].pcb = curr_proc;			//Set the process that is running on the terminal  
		tss.esp0 = EIGHT_MB - (proc_num)*2*FOUR_KB;
	}else{
		tss.esp0 = EIGHT_MB;
	}
	tss.ss0 = KERNEL_DS;
	 //not sure check this
	
	map_virtual_mem(EIGHT_MB + (proc_num)*FOUR_MB);

	
	
//Setup the stack for an iret
	
	asm volatile("pushl %0"
    				:
					:"r"(USER_DS)
					);
	asm volatile("pushl %0;"
    				:
					:"r"(curr_proc->ebp)
					);
	asm volatile("pushl %0;"
					:
					:"r"(curr_proc->flags)
					);
	asm volatile("pushl %0;"
    				:
					:"r"(USER_CS)
					);								
	asm volatile("pushl %0; "
    				:
					:"r"(curr_proc->eip)
					);	
	asm volatile("iret"
    				:
					:
					);	
					
	return -1; //NEVER GETS HERE

}

/*
* execute
 *	 DESCRIPTION: executes a user process
 *   INPUTS: const uint8_t* command
 *   OUTPUTS:
 *   RETURN VALUE: 
 *   SIDE EFFECTS: Remaps the virtual address of video memory to another address
 *				  
 */

int32_t execute(const uint8_t* command){
	
	
cli();
    int i = 0;
	int j = 0;
	//sti();
	//matt tischer said to execute nothing if this is the case
	if(command[i] == ' '){
		return -1;
	}
	
	//checks to find the size of the filename of the executable.
    while(command[i] != ' ' && command[i] != '\0'){
        i++; 
	}
	
	//puts the name of the executable inside of the cmd_exe
    char  cmd_exe[i]; 
    for(j = 0; j < i; j++){
        cmd_exe[j] = command[j];
	}
	//adds null terminator to the end of this filename
    cmd_exe[i] = '\0';   
	
	uint32_t x = 0;
	uint32_t y = 0;
	uint32_t temp = 0;
	temp = i;
	while(command[i] == ' '){
		x++;
		i++;
	}
	while(command[i] != '\0'){
		i++;
	}

	
	//checks to see if the filename exists, if it doesn't return -1
	dentry_t cur_exe;
	if(read_dentry_by_name((uint8_t*)cmd_exe, &cur_exe) == -1){
		return -1;
	}
	
	//checks to see if the 4 bytes of the file match this magic number, if it doesn't return -1
	uint8_t buf[28];
	if(read_data(cur_exe.inode, 0, (uint8_t*)buf, 28) == -1){
		return -1;
	}
	if(buf[0] != 0x7F || buf[1] != 0x45 || buf[2] != 0x4C || buf[3] != 0x46){
		return -1;
	}

	
	//Initializing the pcb
	pcb_t *process;
	

	int p;
	int proc_num;
	
	//Get the process id for process, if there's room less than MAX_NUM_OF_PROCESSES running
	if((proc_num = get_proc_num()) != -1){
	//
		process = (pcb_t *)(EIGHT_MB - 2*FOUR_KB*(proc_num+1));
		process->proc_num = proc_num;	
			//GET ARGS
		for(p = 0; p < 150; p++){
				process->args[p] = '\0';
		}	
		
		for(y = 0; y < (i- temp - x); y++){
			process->args[y] = command[temp + x + y];
		}
		
		
		map_virtual_mem(EIGHT_MB + (proc_num)*FOUR_MB);
		
		num_processes++;
	}else{
		return -1;
	}
	
	

	//copys the first 27 bytes into program image, this is separated so 
	//we can compare the magic numbers above
	memcpy((void*)PROGRAM_IMAGE_ADDR,  (void*)buf,27);
	
	
	

	uint8_t buf2;
	uint32_t offset = 28;
	//copys the rest of the image
	uint16_t uds=0x2B;
	while(read_data(cur_exe.inode, offset, &buf2, 1) > 0){
		
		memcpy((void*)(PROGRAM_IMAGE_ADDR + offset),  &buf2,1);
		offset++;
		
	}


	
	int k;
	for(k = 0; k < 8; k++){
		process->file_array[k].file_ops =NULL;
		process->file_array[k].inode = NULL;
		process->file_array[k].file_pos = 0;
		process->file_array[k].flags = 0;
	
	}
	
	process->file_array[0].file_ops=&fops_term;
	process->file_array[1].file_ops=&fops_term;

	
	
	if(term_process_info[curr_terminal] == 0){
		process->parent_pcb = NULL;
	}else{
		process->parent_pcb = get_process();
	}
	

	process->parent_term = curr_terminal;
	terminal_screens[curr_terminal].pcb = process;
	term_process_info[curr_terminal]++;

	
	
	uint32_t eip = (buf[27] << 24) | (buf[26] << 16) |(buf[25] << 8) |(buf[24]);
	
	uint32_t ebp;
	uint32_t esp;
	uint32_t flags=0x200;
	asm volatile("movl %%ebp, %0; movl %%esp, %1;"
					:"=g"(ebp), "=g"(esp)
					:
					);

	process->esp = esp;
	process->eip = eip;
	//ebp=0x83FFF00;
	ebp = 0x8400000-4;
	tss.ss0 = KERNEL_DS;
	//tss.esp0 = 0x800000 - (num_processes-1)*2*4096; //not sure check this -- minus process*8KB
	tss.esp0 = 0x800000 - (proc_num)*2*4096;
	process->flags = flags;
	process->ebp = ebp;
	//process->term_ebp = ebp;
    asm volatile("pushl %0"
    				:
					:"r"(USER_DS)
					);
	asm volatile("pushl %0;"
    				:
					:"r"(process->ebp)
					);
	asm volatile("pushl %0;"
					:
					:"r"(flags)
					);
	asm volatile("pushl %0;"
    				:
					:"r"(USER_CS)
					);								
	asm volatile("pushl %0; "
    				:
					:"r"(process->eip)
					);	

	asm volatile("movw %0, %%bx"::"b"(uds));
	asm volatile("movw %bx, %ds");
	//asm volatile("movw %0, %%ds"::"b"(uds));
	asm volatile("movl %0, %%ebp"::"b"(ebp));
	sti();
	asm volatile("iret;" : :);
	//158 of the manual, in line assembly
	asm volatile("EXECUTE_RETURN:"
					:
					:
					);
	uint32_t eax;
	asm volatile("movl %%eax, %0"
					:"=g"(eax)
					:
					);
	return eax;
	/*return 0-255 if program executes a halt
	  return 256 if program dies because of an exception*/
	
	/*return -1 when command cannot be executed
	EX. program does not exist or file is not executable*/
return 0;
}

/*
* execute
 *	 DESCRIPTION: executes a user process
 *   INPUTS: const uint8_t* command
 *   OUTPUTS:
 *   RETURN VALUE: 
 *   SIDE EFFECTS: Remaps the virtual address of video memory to another address
 *				  
 */

int32_t read(int32_t fd, void* buf, int32_t nbytes){
	//return number of bytes read
	//return x;
	//RTC returns 0 only after interrupt has occurred
	//Also return 0 when end of file has been reached. 
	if(fd > 7 || fd < 0){
		return -1;
	}
	pcb_t* current_process;
	current_process = get_process();
	file_t* curr_array = &(current_process->file_array[fd]);
	file_operations_t* curr_operation = curr_array->file_ops;

	if(buf==NULL)
	return -1;
	
	int blah = curr_operation->read(fd, buf, nbytes); 
	curr_array->file_pos += blah;
	
	return blah; 
}

/*
* execute
 *	 DESCRIPTION: executes a user process
 *   INPUTS: const uint8_t* command
 *   OUTPUTS:
 *   RETURN VALUE: 
 *   SIDE EFFECTS: Remaps the virtual address of video memory to another address
 *				  
 */

int32_t write(int32_t fd, const void* buf, int32_t nbytes){
	//FOR RTC returns number of bytes written
	//return x;
	/*fails when writes to regular files, system is read-only*/
	if(fd > 7 || fd < 0){
		return -1;
	}
		
	pcb_t* current_process;
	current_process = get_process();
	file_t* curr_array = &(current_process->file_array[fd]);
	file_operations_t* curr_operation = curr_array->file_ops;

	if(buf==NULL)	
		return -1;
	if(nbytes==NULL)
		return -1;
		
	return curr_operation->write(fd, buf, nbytes); 
 
}

/*
* execute
 *	 DESCRIPTION: executes a user process
 *   INPUTS: const uint8_t* command
 *   OUTPUTS:
 *   RETURN VALUE: 
 *   SIDE EFFECTS: Remaps the virtual address of video memory to another address
 *				  
 */

int32_t open(const uint8_t* filename){
	pcb_t* current_process;
	dentry_t temp_dentry;
	current_process=get_process();

	int i = 0;
	//check to see if the file is empty or not

	if(read_dentry_by_name((uint8_t*)filename, &temp_dentry) == -1)
		return -1;

	while(i<8 && (current_process->file_array[i].file_ops != NULL)){
	i++;}
	
	
	if(i<8){
	//RTC
	if(temp_dentry.filetype==0){
		current_process->file_array[i].file_ops=&file_op[2];
		current_process->file_array[i].inode=temp_dentry.inode;
		current_process->file_array[i].file_pos=0;
		current_process->file_array[i].flags=1;
		
		file_operations_t* curr_operation = current_process->file_array[i].file_ops;
		curr_operation->open((uint8_t*) filename);
		
		return i;
	}
	//DIRECTORY
	else if(temp_dentry.filetype==1){
		current_process->file_array[i].file_ops=&file_op[3];
		current_process->file_array[i].inode=temp_dentry.inode;
		current_process->file_array[i].file_pos=0;
		current_process->file_array[i].flags=1;
		
		file_operations_t* curr_operation = current_process->file_array[i].file_ops;
		curr_operation->open((uint8_t*) filename);
		
		return i;
	}
	//FILE 
	else if(temp_dentry.filetype==2){
		current_process->file_array[i].file_ops=&file_op[4];
		current_process->file_array[i].inode=temp_dentry.inode;
		current_process->file_array[i].file_pos=0;
		current_process->file_array[i].flags=1;
		
		file_operations_t* curr_operation = current_process->file_array[i].file_ops;
		curr_operation->open((uint8_t*) filename);
		
		return i;
	}
	else
	return -1; //file does not exist or no descriptors are free
	}
	
	return -1;
}

/*
* execute
 *	 DESCRIPTION: executes a user process
 *   INPUTS: const uint8_t* command
 *   OUTPUTS:
 *   RETURN VALUE: 
 *   SIDE EFFECTS: Remaps the virtual address of video memory to another address
 *				  
 */

int32_t close(int32_t fd){
	if(fd > 7 || fd < 0){
		return -1;
	}
	pcb_t* current_process;
    current_process = get_process();

	file_t* curr_array = &(current_process->file_array[fd]);
	
	//never opened, error
	if(curr_array->flags == NULL){
		return -1;
	}
	
	curr_array->file_ops = NULL;
	curr_array->inode = NULL;
	curr_array->file_pos = 0;
	curr_array->flags = 0;

	return 0;
}

/*
* execute
 *	 DESCRIPTION: executes a user process
 *   INPUTS: const uint8_t* command
 *   OUTPUTS:
 *   RETURN VALUE: 
 *   SIDE EFFECTS: Remaps the virtual address of video memory to another address
 *				  
 */

void init_ops(){
	file_op[0].read=&terminal_read;
	file_op[0].write=NULL;
	file_op[0].open=&terminal_open;
	file_op[0].close=&terminal_close;
	file_op[1].read=NULL;
	file_op[1].write=&terminal_write;
	file_op[1].open=&terminal_open;
	file_op[1].close=&terminal_close;
	file_op[2].read=&read_rtc;
	file_op[2].write=&write_rtc;
	file_op[2].open=&open_rtc;
	file_op[2].close=&close_rtc;
	file_op[3].read=&dir_read;
	file_op[3].write=NULL;
	file_op[3].open=&dir_open;
	file_op[3].close=&dir_close;
	file_op[4].read=&file_read;
	file_op[4].write=NULL;
	file_op[4].open=&file_open;
	file_op[4].close=&file_close;
	int i=0;
	for(i=0; i<2;i++){
	pcbarray[i].file_array[0].file_ops=&file_op[0];
	pcbarray[i].file_array[1].file_ops=&file_op[1];
	}
}

/*
* execute
 *	 DESCRIPTION: executes a user process
 *   INPUTS: const uint8_t* command
 *   OUTPUTS:
 *   RETURN VALUE: 
 *   SIDE EFFECTS: Remaps the virtual address of video memory to another address
 *				  
 */

int32_t vidmap (uint8_t** screen_start)
{
//checking for  bounds if vir add is more than 132 (128+4) or lesser then 128
	 if((int32_t)screen_start < VIRTUAL_ADDR || (int32_t)screen_start > VIRTUAL_ADDR + IMG_SIZE)
        {
                return -1; 
        }

	*screen_start=(uint8_t *) 0xB8000;
	return 0;
}

/*
* execute
 *	 DESCRIPTION: executes a user process
 *   INPUTS: const uint8_t* command
 *   OUTPUTS:
 *   RETURN VALUE: 
 *   SIDE EFFECTS: Remaps the virtual address of video memory to another address
 *				  
 */

int32_t getargs(uint8_t* buf, int32_t nbytes)
{

	pcb_t* pcb_ptr;
	pcb_ptr = get_process();
	uint8_t* args = pcb_ptr->args;
	if(strlen((int8_t*)args)+1 > nbytes)
		return -1;
	
	strncpy((int8_t*) buf,(int8_t*) args, nbytes);
	
	return 0;
}


/*
* execute
 *	 DESCRIPTION: executes a user process
 *   INPUTS: const uint8_t* command
 *   OUTPUTS:
 *   RETURN VALUE: 
 *   SIDE EFFECTS: Remaps the virtual address of video memory to another address
 *				  
 */

/*READ ONLY SYSTEM, THESE FAIL*/
//complete 
int32_t set_handler(int32_t signum, void* handler_address){
	return -1; 
}
/*
* execute
 *	 DESCRIPTION: executes a user process
 *   INPUTS: const uint8_t* command
 *   OUTPUTS:
 *   RETURN VALUE: 
 *   SIDE EFFECTS: Remaps the virtual address of video memory to another address
 *				  
 */

//complete
int32_t sigreturn(void){
	return -1;
}

/*
* execute
 *	 DESCRIPTION: executes a user process
 *   INPUTS: const uint8_t* command
 *   OUTPUTS:
 *   RETURN VALUE: 
 *   SIDE EFFECTS: Remaps the virtual address of video memory to another address
 *				  
 */

int get_proc_num(){
int i =0;
	while(i<MAX_NUM_OF_PROCESSES && (process_array[i] != 0)){
	i++;}
	
	if(i<MAX_NUM_OF_PROCESSES){
		process_array[i] = 1;
		return i;
	}else{
		return -1;
	}
}


pcb_t* get_process(void){

	uint32_t esp;

	asm volatile("movl %%esp, %0"
					:"=b"(esp)
					:
					);


	return (pcb_t*) (esp & 0xFFFFE000);

}

