#ifndef _MP3_HANDLER_H
#define _MP3_HANDLER_H

#include "lib.h"

	extern void divide_exception_handler();
	extern void debug_exception_handler();
	extern void nmi_interrupt_handler();
	extern void breakpoint_exception_handler();
	extern void overflow_exception_handler();
	extern void boundrange_exception_handler();
	extern void invalid_op_exception_handler();
	extern void no_device_exception_handler();
	extern void doublefault_exception_handler();
	extern void coprocessor_seg_overrun_handler();
	extern void invalidTSS_exception_handler();
	extern void no_segment_handler();
	extern void stackfault_exception_handler();
	extern void generalprot_exception_handler();
	extern void pagefault_exception_handler();
	extern void floatingpoint_error_handler();
	extern void alignmentcheck_exception_handler();
	extern void machinecheck_exception_handler();
	extern void simd_floatingpoint_exception_handler();
	extern void system_call_handler();
	int rtc_flag;

#endif


