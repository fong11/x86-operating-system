
#include "keyboard.h"
#include "lib.h"
#include "i8259.h"
#include "syscalls.h"
#include "paging.h"

//uint8_t Enter; 

file_operations_t fops_term = {terminal_read, terminal_write, NULL, NULL};

/*
 * keyboard_handler
 *	 DESCRIPTION: Reads from port 0x60 and displays the appropriate key after obtaining it from the kbdus[] array.
				  This funtion is called from the generic low level exception handler.
 *   INPUTS: 
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *   SIDE EFFECTS: prints to terminal the character read from port x60 (keyboard).
 */
 /*Scancodes taken from 
 http://www.osdever.net/bkerndev/Docs/keyboard.htm
 */
 static char* video_mem;
 static unsigned int SHIFT;
 static unsigned int CTRL, ALT, F1, F2, F3;
 static unsigned int CAPS;
 unsigned char *buffer;
 int curr_terminal =0;

 static int count;

unsigned char __attribute__((aligned(FOUR_KB))) screen_buf[3][FOUR_KB];
 
terminal_screen_t terminal_screens[3];
 
 
 //bs is to print whole new thing in case of backspace
 int i ,k , bs;
 int ret_addr;
 int new_line_flag;
 int bks_flag;
 int ent_flag;
 int buffer_max;
 volatile int typed = 0;
 
 //Scancode for keyboard without SHIFT or CTRL or CAPS combination.
 uint8_t kdbus[] = {
	0,  0x1B,
	'1', '2', '3', '4', '5', '6', '7', '8',	'9', '0', '-', '=', '\b',
	'\t', 'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', '[', ']',
	'\n',
    0, 
	'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';', '\'',
	'`',
	0, //this for left shift 0x2A
	'\\',
	'z', 'x', 'c', 'v', 'b', 'n', 'm', ',', '.', '/', 0, 
	'*', 
	0, ' ', 
	0, 
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
	0,
	0, 
	'7', '8', '9', '-', 
	'4', '5', '6', '+', 
	'1', '2', '3', 
	'0', '.', 
	0, 0, 0, 
	0, 0, 
	
};

//Scancode for keyboard with SHIFT or CTRL or CAPS combination.
uint8_t kdbus_shift[] = {
	0,  0x1B, // Esc
	'!', '@', '#', '$', '%', '^', '&', '*',	'(', ')', '_', '+', '\b',
	'\t', 'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', '{', '}',
	'\n',
    0, // 0x1D - Left ctrl
	'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', ':', '"',
	'~',
	0, // 0x2A - Left shift
	'|',
	'Z', 'X', 'C', 'V', 'B', 'N', 'M', '<', '>', '?'
};

/*
 * terminal_open
 *	 DESCRIPTION: Terminal_open function enables the keyboard and opens the keyboard port in 
 *				  8259 pic. 
 *   INPUTS:      none
 *   OUTPUTS: 	  none
 *   RETURN VALUE:none
 *   SIDE EFFECTS:Enables keyboard on 8259 master pic. .
 */
uint32_t terminal_open(){
	enable_irq(1);
	video_mem = (char *)VIDEO;
	
	
	curr_terminal = 0;
	terminal_screens[0].key_count = 0;
	buffer = terminal_screens[0].key_buf;
	count = terminal_screens[0].key_count;
	
	pcb_t* curr_proc;
	curr_proc = get_process();
	terminal_screens[0].pcb = curr_proc;
	terminal_screens[1].pcb = NULL;
	terminal_screens[2].pcb = NULL;
	
	//Clear screens
	initialise_terminals();
	terminal_screens[0].root_shell = 1;
	
	SHIFT = 0;
	CTRL = 0;
	CAPS = 0;
	ALT = 0;
	count =0 ;
 //bs is to print whole new thing in case of backspace
	bs = 0;
	new_line_flag =0 ;
	bks_flag = 0;
	ent_flag = 0;
	buffer_max = 0 ;
	sti();
	return 0;
}

/*
 * keyboard_handler
 *	 DESCRIPTION: Reads from port 0x60 and displays the appropriate key after obtaining it from the kbdus[] array.
 *				  This funtion is called from the generic low level exception handler. It also keeps track of the screen 
 *				  location for this purpose. It also supports scrolling. It also supports backspace and line-buffered 
 *				  input. That is, when a read system call is made to the terminal, it only return data after the user 
 *				  presses enter. Until that time, data is buffered in the driver and edited appropriately. The size of
 *				  buffer should be 128 characters.
 *   INPUTS: 
 *   OUTPUTS: Character on screen / terminal
 *   RETURN VALUE: none
 *   SIDE EFFECTS: prints to terminal the character read from port x60 (keyboard).
 */
void keyboard_handler()
{
	cli();
	
	asm volatile("movl -20(%%ebp), %0"
				:"=b"(ret_addr)
				:
				);
	
	//Get the keycode from the port
	unsigned char key_code;
	key_code = inb(0x60);
	
	
	if(key_code == ALT_KEY){
			ALT = 1;
	}
	
	//ALT released
	if(key_code == 0xB8){
			ALT = 0;
	}
	
	if(key_code == F1_KEY){
			F1 = 1;
	}
	
	//F1 release
	if(key_code == 0xBB){
			F1 = 0;
	}
	
	if(key_code == F2_KEY){
			 F2 = 1;
	}
	
	//F2 release
	if(key_code == 0xBC){
			F2 = 0;
	}
	
	if(key_code == F3_KEY){
			 F3 = 1;
	}
	
	//F3 release
	if(key_code == 0xBD){
			F3 = 0;
	}
	
	//Terminal switching
	if(ALT && F1){

		if(curr_terminal != 0){
		switch_terminals(0);
		
		if(terminal_screens[0].root_shell == 0){
			terminal_screens[0].root_shell =1;
			curr_terminal = 0;
			send_eoi(1);
			sti();
			execute((uint8_t*)"shell");
		}
		
		send_eoi(1);
		sti();
		
		}
	}
	
	if(ALT && F2){
	
		if(curr_terminal != 1){
			switch_terminals(1);
			if(terminal_screens[1].root_shell == 0){
				terminal_screens[1].root_shell =1;
				curr_terminal = 1;
				send_eoi(1);
				sti();
				execute((uint8_t*)"shell");
			}
		
		
		
		}
	}
	
	if(ALT && F3){
	
		if(curr_terminal != 2){
		
		switch_terminals(2);
		

		if(terminal_screens[2].root_shell == 0){
			terminal_screens[2].root_shell = 1; 
			curr_terminal = 2;
			send_eoi(1);
			sti();
			execute((uint8_t*)"shell");
		}

		}
	}
	
	
	 if(key_code == L_SHIFT || key_code == R_SHIFT) 	
		SHIFT = 1;

	//if(key_code == 0X3A)
		//CAPS =1;
	//L_SHIFT or R_SHIFT key released (key_code | 80) 
	if(key_code == 0xAA || key_code == 0xB6)
		{
			SHIFT = 0;
		}
	/*
Extra functionality of CAPS lock : 
	if(CAPS == 1 && key_code == 0xBA)
		CAPS =0;
		
	if(CAPS ==1 && key_code >= 0x01 && key_code<=0x35)
		{
			if(kdbus_shift[key_code] != 0)
			putc(kdbus_shift[key_code]);
			//if(count <129)
			//{
			//	buffer[count] = printf('%c',kdbus_shift[key_code]);
			//	count++;
			//}
		}
	*/
	if(SHIFT ==1 && key_code >= 0x01 && key_code<=0x35){
		
		if(kdbus_shift[key_code] != 0)
		{
		//putting into buffer , and avoiding enter
			if(key_code != ENTER && key_code != 0x9C && key_code != BACKSPACE && key_code != 0x8E)
			{
				buffer[count] = kdbus_shift[key_code];
			
				move_cursor();
				putc(buffer[count]);
				count++; 
				if(new_line_flag ==1)
				{
					
					
					if(screen_y == 24)
						{
						scroll();
						screen_y = 24;
						screen_x = 0;
						move_cursor();
						}
							
					else
					{
					printf("\n");
					move_cursor();
					}
					new_line_flag = 0;
				}
				
				
				
				if(screen_x == 79)
				{
				 new_line_flag = 1;
				}
			
			}
		}
	}

	if(key_code == CTRL_KEY){
		CTRL =1;
	}

	if(CTRL ==1 && key_code == 0x26)
		{
		clear();
		move_cursor();
		memset(&buffer[0], 0, count);
		count = 0;
		
		}
	//released
	if(key_code== 0x9D){
		CTRL = 0;
	}
	//normal key press
	if(SHIFT==0 && CTRL == 0 && CAPS == 0 && key_code != (key_code | 0x80) && kdbus[key_code] != 0 && key_code != 0x01 && key_code !=0x37 && key_code != 0x52 
	&& key_code != 0x47 && key_code != 0x49 && key_code != 0x53 && key_code != 0x4f && key_code != 0x51 && key_code != 0x4b 
	&& key_code != 0x4d && key_code != 0x48 && key_code != 0x50 && key_code != 0x1d && key_code != 0x9d && key_code != 0x0f && key_code != 0x3A)  
		{
		
			//putc(kdbus[key_code]);
		//putting into buffer , and avoiding enter ,backspace
			if(key_code != ENTER && key_code != 0x9C && key_code != BACKSPACE && key_code != 0x8E)
			{
				buffer[count] = kdbus[key_code];
	//writing from buffer this is extremely helful when tackling the case of special keys
	//suck as backspace or delete or home etc. 
	//updating the position on our cursors
				move_cursor();
				putc(buffer[count]);
				count++; 
				print_chr_on_screen('c');
			
			}
		}
//Backspace implementation
	if(key_code == BACKSPACE)
	{	
		backspace_fn();
		
	}

		
//when pressed enter the buffer prints and if reached the end of line 
//it will simply scroll it up.
	if(key_code == ENTER)
	{	
		new_line_flag = 0;
		typed =1;
		ent_flag = 1;
		if(ent_flag == 1)
		{
		buffer[count] = '\0';
		ent_flag = 0;
		
		}
		
//Checking for screen bounds, in this case the end of screen wrt y co-ordinate or rows
//Maximum Y coord value possible is 25 pixels.(24, since it starts from 0) 
		if(screen_y == 24)
		{
			scroll();
			screen_x = 0;
			screen_y = 24;
			move_cursor();
		}
		else
		{
			screen_y++;
			screen_x = 0;
			move_cursor();
		}
	//checking is buffer is already empty
		if(buffer[0] != '\0' && buffer[count-1] != '\0')
		{
	//ASK TA		
			if(count >= BUFFER_MAX)
			{
				count = BUFFER_MAX;
				buffer[count] = '\n';
				buffer_max = 1;
			}
		
			else
			{
				buffer[count+1] = '\0';
				count++;
		//taking care of conditions when we use backspaces so we clear the buffer still
		//after null termination.
				for(k = count ; k < BUFFER_MAX ; k++)
				{
					
					buffer[k] = '\0';
				}
			}
		
		}
		//TESTING
		//terminal_write(0, buffer, count);
		//terminal_read(buffer, count);
	}	
		
		send_eoi(1);
	//typed = 1;
	sti();
		
}

uint32_t terminal_read(int32_t fd, void* buf, int32_t nbytes)
{

	sti();
	int read_count = 0;
	
	int hack = 0;
	
	
	pcb_t* curr_proc;
	curr_proc = get_process();
	
	int term = curr_proc->parent_term;
	//int original_term = curr_terminal;
	
	typed =0;
	while(typed ==0){
	
		hack++;
	}
	
		for(i = 0; i<nbytes ; i++)
			{
				*((char*)(buf)+i) = terminal_screens[term].key_buf[i];
			}
			read_count = count;
		//as soon as copying to external buffer is complete, we clear the buffer
			//memset(&buffer[0], 0, count);
			count = 0;
	//GET RID OF THIS ITS A TESTING
		//terminal_write(buf, read_count);
			
		if (read_count != 0 ) 
			read_count = read_count-1;
		if(buffer_max == 1)
			{
				read_count = BUFFER_MAX;
				buffer_max = 0;
			}
		//printf("%d",read_count);
		return read_count ;
		
}

uint32_t terminal_write(int32_t fd,const void* buf, int32_t nbytes)
{
	
	
	
	
	pcb_t* curr_proc;
	curr_proc = get_process();
	
	int term = curr_proc->parent_term;
	//int original_term = curr_terminal;
	
	if(term != curr_terminal){
		map_vid_mem((uint32_t *) screen_buf[term]);
	}	
	
	//switch_terminals(term);
	
	int check =0;
	char current_char;

	if(screen_y == 23){
		check++;
	}

		//if(nbytes != 0 && nbytes <128)
			//nbytes--;	

		//if(*((char*)(buf)+128) == '\0')
			//{
				//printf("%s\n, yolo");
				//nbytes++;
			//}
		int newtemp = 0;
		for(i = 0; i< nbytes ; i++)
			{
				current_char = *((char*)(buf)+i);

				if((screen_y == 24) && (current_char == '\n')){
					new_line_flag = 1;
					newtemp = 1;
				}
			/*	if((screen_y == 23) && (current_char == '\n')){
					newtemp = 1;
					current_char = ' ';
				}*/
				if((current_char) != '\n' || newtemp == 0){
					putc(current_char);
				}

				print_chr_on_screen(current_char);

			}
			//printf("%d",nbytes);
			if(term != curr_terminal){
				map_vid_mem((uint32_t *) VIDEO);
			}	
	//switch_terminals(original_term);
	
		return 0;
}

void backspace_fn()
{
if (buffer != 0 && count != 0 && buffer[count] != '\n')
		{
			ent_flag = 1;
			 // only deleting the text provided by user 
			 //not the once by machine or terminal/ 
					buffer[count] = '\0';
					count--;
					move_cursor();
					if(screen_x == 0)
					{
						screen_x = NUM_COLS - 1;
						screen_y--;
						move_cursor();
					} 
					else screen_x--;
					move_cursor();
					*(video_mem + ((NUM_COLS*screen_y + screen_x) << 1)) = 0;
					*(video_mem + ((NUM_COLS*screen_y + screen_x) << 1) + 1) = NEW_ATTRIB;
					//move_cursor();
		
		}
		
		
		if(screen_x == 79)
			{
				new_line_flag =1;
				bks_flag = 1;
			}
			
		if(screen_x != 79 && bks_flag ==1)
				new_line_flag =0;
}


void print_chr_on_screen(char c)
{
	if(new_line_flag ==1)
	{
		if(screen_y == 24)
		{
			
			if(screen_x != 0){
				scroll();
			}
				screen_y = 24;
				screen_x = 0;
				move_cursor();
			
		}
							
		else
		{
			//if the character is not a new line
			if(c != '\n'){
				putc('\n');
			}
			move_cursor();
		}
			new_line_flag = 0;
			
	}
	
	if(screen_x == 79)
	{
		if(c != '\n'){
		 new_line_flag = 1;
		 }

	}

}


void initialise_terminals(){
	
	int i;
	
	for(i=0;i<3;i++){
		clear_screen(i);
		terminal_screens[i].root_shell = 0;
	}
	
	
}

void switch_terminals(int term_num){
	
	//uint8_t c;
	
	
	if(term_num != curr_terminal){
	
	
	
	terminal_screens[curr_terminal].screen_x = screen_x;
	terminal_screens[curr_terminal].screen_y = screen_y;
	terminal_screens[curr_terminal].key_count = count; 
	
		for(i=0; i<NUM_ROWS*NUM_COLS; i++) {
			
			//screen_buf[curr_terminal][i] = *((uint8_t*)video_mem+i);
			*(uint8_t *)(screen_buf[curr_terminal] + ((i) << 1)) = *(uint8_t *)(video_mem + ((i) << 1));
			//*(uint8_t *)(screen_buf[curr_terminal].screen_buf + ((i) << 1)) = terminal_screens[term_num].screen_buf[i]; 
			*(uint8_t *)(screen_buf[curr_terminal] + ((i) << 1) + 1) = ATTRIB;
			
			*(uint8_t *)(video_mem + ((i) << 1)) = *(uint8_t *)(screen_buf[term_num] + ((i) << 1));
			*(uint8_t *)(video_mem + ((i) << 1) + 1) = ATTRIB;
			//*((uint8_t *)video_mem+i) = screen_buf[term_num][i];
		
	
		
		}
		
		//map_vid_mem((uint32_t *) terminal_screens[term_num].screen_buf);		
		buffer = terminal_screens[term_num].key_buf;
		count = terminal_screens[term_num].key_count;
		screen_x= terminal_screens[term_num].screen_x;
		screen_y= terminal_screens[term_num].screen_y;
		
		
		curr_terminal = term_num;
		
	}
	
	
	
	
	
	return;
}

void clear_screen(int term)
{
    int32_t i;
    for(i=0; i<NUM_ROWS*NUM_COLS; i++) {
	
        //terminal_screens[term].screen_buf[i] = ' ';
		//term->screen_buf[i] = ' ';
		*(uint8_t *)(screen_buf[term] + (i << 1)) = ' ';
        *(uint8_t *)(screen_buf[term] + (i << 1) + 1) = ATTRIB;
    }
	//	term->screen_x = 0;
		//term->screen_y = 0;
		terminal_screens[term].screen_x = 0;
		terminal_screens[term].screen_y = 0;
}


uint32_t terminal_close()
{
	return 0;
}

void term_putc(uint8_t c, int term)
{
	int x = terminal_screens[term].screen_x;
	int y = terminal_screens[term].screen_y;
	
    if(c == '\n' || c == '\r') {
        y++;
        x=0;
    } else {
        
		screen_buf[term][NUM_COLS*y + x] = c;
		//*(uint8_t *)(video_mem + ((NUM_COLS*y + x) << 1)) = c;
        //*(uint8_t *)(video_mem + ((NUM_COLS*y + x) << 1) + 1) = ATTRIB;
        x++;
        x %= NUM_COLS;
        y = (y + (x / NUM_COLS)) % NUM_ROWS;
    }
	
	terminal_screens[term].screen_x = x;
	terminal_screens[term].screen_y = y;
}


void process_switch(int term){

	//get_process
	pcb_t* curr_proc;
	curr_proc = get_process();
	
	int proc_num = curr_proc->proc_num;
	
	//save registers into that curr_proc
	asm volatile("		\n"
	"movl %%eax, %0		\n"
	"movl %%ebx, %1  	\n"
	"movl %%ecx, %2		\n"
	"movl %%edx, %3		\n"
	"movl %%edi, %4		\n"
	"movl %%esi, %5		\n"
	:"=r"(curr_proc->eax), "=r"(curr_proc->ebx), "=r"(curr_proc->ecx), "=r"(curr_proc->edx), "=r"(curr_proc->edi), "=r"(curr_proc->esi)
	:);
	
	
	terminal_screens[curr_terminal].pcb = curr_proc;
	
	
	curr_proc = terminal_screens[term].pcb;
	
	proc_num = curr_proc->proc_num;
	
	//if(num_processes < MAX_NUM_OF_PROCESSES){
		curr_proc = (pcb_t *)(0x800000 - 2*FOUR_KB*(proc_num+1));
		map_virtual_mem(0x800000 + (proc_num)*0x400000);
	//	num_processes++;
	//}else{
	//	return;
	//}
	
	curr_terminal = term;	
		
	//esp0 and ss0
	tss.ss0 = KERNEL_DS;
	tss.esp0 = 0x800000 - (proc_num)*2*4096;
 
	//restore all of the registers
	asm volatile("		\n"
	"movl %0, %%eax		\n"
	"movl %1, %%ebx		\n"
	"movl %2, %%ecx		\n"
	"movl %3, %%edx		\n"
	"movl %4, %%edi		\n"
	"movl %5, %%esi		\n"
	:
	:"r"(curr_proc->eax), "r"(curr_proc->ebx), "r"(curr_proc->ecx), "r"(curr_proc->edx), "r"(curr_proc->edi), "r"(curr_proc->esi));
	
	//push correct stuff onto stack and call iret
	

	asm volatile("pushl %0"
    				:
					:"r"(USER_DS)
					);
	asm volatile("pushl %0;"
    				:
					:"r"(curr_proc->ebp)
					);
	asm volatile("pushl %0;"
					:
					:"r"(curr_proc->flags)
					);
	asm volatile("pushl %0;"
    				:
					:"r"(USER_CS)
					);								
	asm volatile("pushl %0; "
    				:
					:"r"(ret_addr)
					);	
	asm volatile("iret"
    				:
					:
					);	
	
	

}

