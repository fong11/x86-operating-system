#include "idt.h" 
#include "x86_desc.h" 
#include "assembly_handlers.h" 
#include "lib.h" 

/*implemented using the IA32 reference manual page 156 
the last two 0's of the interrupt gate and trap gate are not used
dpl is 2 bits, see idt struct in the x86_desc.h if confused

task gate might not be necessary since we don't have any programs..?

BUG LOG - used to give & for the handler */
void set_task_gate(int vec, void* handler){ 
    
    idt[vec].reserved3 = 1; 
    idt[vec].reserved2 = 0; 
    idt[vec].reserved1 = 1; 
    idt[vec].size = 0; 
    idt[vec].reserved0 = 0; 
    idt[vec].dpl = 3; 
    idt[vec].present = 1; 
	idt[vec].seg_selector = KERNEL_CS; 
      
    SET_IDT_ENTRY(idt[vec], handler); 
} 
void set_intr_gate(int vec, void* handler){ 

    idt[vec].reserved4 = 0; 
    idt[vec].reserved3 = 0; 
    idt[vec].reserved2 = 1; 
    idt[vec].reserved1 = 1; 
    idt[vec].size = 1; 
    idt[vec].reserved0 = 0; 
    idt[vec].dpl = 0; 
    idt[vec].present = 1; 
    idt[vec].seg_selector = KERNEL_CS; 
	
    SET_IDT_ENTRY(idt[vec], handler); 
} 
  void set_trap_gate(int vec, void* handler){ 

    idt[vec].reserved4 = 0; 
    idt[vec].reserved3 = 1; 
    idt[vec].reserved2 = 1; 
    idt[vec].reserved1 = 1; 
    idt[vec].size = 1; 
    idt[vec].reserved0 = 0; 
    idt[vec].dpl = 0; 
    idt[vec].present = 1; 
    idt[vec].seg_selector = KERNEL_CS;
	
    SET_IDT_ENTRY(idt[vec], handler); 
} 

