#ifndef _PAGING_H
#define _PAGING_H

#include "types.h"
#include "lib.h"
#include "x86_desc.h"
#include "syscalls.h"


#define ONLY_RW 0|2
#define NUM_ENTRIES	1024
#define FOUR_KB 4096

#define SET_PRESENT_RW 0x3
#define SET_PRESENT_SUP_RW 0x7
#define SET_4MB_PAGES 0x80
#define SET_NOT_PRES_SUP_RW 0x6
#define FOUR_MB 0x400000
#define EIGHT_MB 0x800000

#define ENABLE_PAGING 0x80000000
#define ENABLE_4MB_PAGES 0x00000010
#define KERNEL_ADDRESS 0x400000

//extern uint32_t * page_directory;
extern void initialize_paging();
extern void map_virtual_mem(uint32_t);
void map_vid_mem(uint32_t* term);

#endif
