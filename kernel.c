/* kernel.c - the C part of the kernel
 * vim:ts=4 noexpandtab
 */
#include "idt.h"
#include "multiboot.h"
#include "x86_desc.h"
#include "lib.h"
#include "i8259.h"
#include "debug.h"
#include "assembly_handlers.h"
#include "mp3_handler.h"
#include "keyboard.h"
#include "paging.h"
#include "rtc.h"
#include "filesystem.h"
#include "syscalls.h"
/* Macros. */
/* Check if the bit BIT in FLAGS is set. */
#define CHECK_FLAG(flags,bit)   ((flags) & (1 << (bit)))

/* Check if MAGIC is valid and print the Multiboot information structure
   pointed by ADDR. */
void
entry (unsigned long magic, unsigned long addr)
{
	multiboot_info_t *mbi;

	/* Clear the screen. */
	clear();

	/* Am I booted by a Multiboot-compliant boot loader? */
	if (magic != MULTIBOOT_BOOTLOADER_MAGIC)
	{
		printf ("Invalid magic number: 0x%#x\n", (unsigned) magic);
		return;
	}

	/* Set MBI to the address of the Multiboot information structure. */
	mbi = (multiboot_info_t *) addr;

	/* Print out the flags. */
	printf ("flags = 0x%#x\n", (unsigned) mbi->flags);

	/* Are mem_* valid? */
	if (CHECK_FLAG (mbi->flags, 0))
		printf ("mem_lower = %uKB, mem_upper = %uKB\n",
				(unsigned) mbi->mem_lower, (unsigned) mbi->mem_upper);

	/* Is boot_device valid? */
	if (CHECK_FLAG (mbi->flags, 1))
		printf ("boot_device = 0x%#x\n", (unsigned) mbi->boot_device);

	/* Is the command line passed? */
	if (CHECK_FLAG (mbi->flags, 2))
		printf ("cmdline = %s\n", (char *) mbi->cmdline);

	if (CHECK_FLAG (mbi->flags, 3)) {
		int mod_count = 0;
		int i;
		module_t* mod = (module_t*)mbi->mods_addr;
		while(mod_count < mbi->mods_count) {
			printf("Module %d loaded at address: 0x%#x\n", mod_count, (unsigned int)mod->mod_start);
			printf("Module %d ends at address: 0x%#x\n", mod_count, (unsigned int)mod->mod_end);
			printf("First few bytes of module:\n");
			for(i = 0; i<16; i++) {
				printf("0x%x ", *((char*)(mod->mod_start+i)));
			}
			printf("\n");
			mod_count++;
		}
	}

	/* Bits 4 and 5 are mutually exclusive! */
	if (CHECK_FLAG (mbi->flags, 4) && CHECK_FLAG (mbi->flags, 5))
	{
		printf ("Both bits 4 and 5 are set.\n");
		return;
	}

	/* Is the section header table of ELF valid? */
	if (CHECK_FLAG (mbi->flags, 5))
	{
		elf_section_header_table_t *elf_sec = &(mbi->elf_sec);

		printf ("elf_sec: num = %u, size = 0x%#x,"
				" addr = 0x%#x, shndx = 0x%#x\n",
				(unsigned) elf_sec->num, (unsigned) elf_sec->size,
				(unsigned) elf_sec->addr, (unsigned) elf_sec->shndx);
	}

	/* Are mmap_* valid? */
	if (CHECK_FLAG (mbi->flags, 6))
	{
		memory_map_t *mmap;

		printf ("mmap_addr = 0x%#x, mmap_length = 0x%x\n",
				(unsigned) mbi->mmap_addr, (unsigned) mbi->mmap_length);
		for (mmap = (memory_map_t *) mbi->mmap_addr;
				(unsigned long) mmap < mbi->mmap_addr + mbi->mmap_length;
				mmap = (memory_map_t *) ((unsigned long) mmap
					+ mmap->size + sizeof (mmap->size)))
			printf (" size = 0x%x,     base_addr = 0x%#x%#x\n"
					"     type = 0x%x,  length    = 0x%#x%#x\n",
					(unsigned) mmap->size,
					(unsigned) mmap->base_addr_high,
					(unsigned) mmap->base_addr_low,
					(unsigned) mmap->type,
					(unsigned) mmap->length_high,
					(unsigned) mmap->length_low);
	}

	/* Construct an LDT entry in the GDT */
	{
		seg_desc_t the_ldt_desc;
		the_ldt_desc.granularity    = 0;
		the_ldt_desc.opsize         = 1;
		the_ldt_desc.reserved       = 0;
		the_ldt_desc.avail          = 0;
		the_ldt_desc.present        = 1;
		the_ldt_desc.dpl            = 0x0;
		the_ldt_desc.sys            = 0;
		the_ldt_desc.type           = 0x2;

		SET_LDT_PARAMS(the_ldt_desc, &ldt, ldt_size);
		ldt_desc_ptr = the_ldt_desc;
		lldt(KERNEL_LDT);
	}

	/* Construct a TSS entry in the GDT */
	{
		seg_desc_t the_tss_desc;
		the_tss_desc.granularity    = 0;
		the_tss_desc.opsize         = 0;
		the_tss_desc.reserved       = 0;
		the_tss_desc.avail          = 0;
		the_tss_desc.seg_lim_19_16  = TSS_SIZE & 0x000F0000;
		the_tss_desc.present        = 1;
		the_tss_desc.dpl            = 0x0;
		the_tss_desc.sys            = 0;
		the_tss_desc.type           = 0x9;
		the_tss_desc.seg_lim_15_00  = TSS_SIZE & 0x0000FFFF;

		SET_TSS_PARAMS(the_tss_desc, &tss, tss_size);

		tss_desc_ptr = the_tss_desc;

		tss.ldt_segment_selector = KERNEL_LDT;
		tss.ss0 = KERNEL_DS;
		tss.esp0 = 0x800000;
		ltr(KERNEL_TSS);
	}
	lidt(idt_desc_ptr); //bug log forgot to put this
		
	//these are defined on page 29 on the notes and on page 166 of the ia32 manual
	set_trap_gate(0,divide_exception_handle);
	set_trap_gate(1,debug_exception_handle);
	//set_intr_gate(2,&nmi_interrupt_handle);
	set_intr_gate(3,breakpoint_exception_handle);
	set_trap_gate(4,overflow_exception_handle);
	set_trap_gate(5,boundrange_exception_handle);
	set_trap_gate(6,invalid_op_exception_handle);
	set_trap_gate(7,no_device_exception_handle);
	set_task_gate(8,doublefault_exception_handle);
	set_trap_gate(9,coprocessor_seg_overrun_handle);
	set_trap_gate(10,invalidTSS_exception_handle);
	set_trap_gate(11,no_segment_handle);
	set_trap_gate(12,stackfault_exception_handle);
	set_trap_gate(13,generalprot_exception_handle);
	set_trap_gate(14,pagefault_exception_handle);
	set_trap_gate(16,floatingpoint_error_handle);
	set_trap_gate(17,alignmentcheck_exception_handle);
	set_trap_gate(18,machinecheck_exception_handle);
	set_trap_gate(19,simd_floatingpoint_exception_handle);
	set_intr_gate(0x20, pit_handle); //32
	set_intr_gate(0x21, keyboard_handle); //33
	set_intr_gate(0x28, rtc_handle);	  //38 
	set_intr_gate(0x80,system_call_handle); //128
	idt[128].dpl=3;
	/* Init the PIC */
	i8259_init();
	initialize_paging();
	clear(); // clears everything
	terminal_open();
	init_ops();
	module_t* mod = (module_t*)mbi->mods_addr;
	//file_open((int8_t*)"verylargetxtwithverylongname.tx");
	
	//file_open((uint8_t*)"grep");
	
	bootblock_addr = (bootblock_t*) mod->mod_start;
	enable_irq(0);
	enable_irq(2); // slave pic
	
	
	execute((uint8_t*)"shell");

	/*
	int8_t kbuf[32];
	int num_char;
	num_char = terminal_read(0,kbuf, 32);
	
	printf("%d\n", num_char);
	*/
	//printf("execute works!");
	//TESTING DO NOT REMOVE! filesystems.h

	//this tests read_dir
	/*int8_t buf[32];
	int32_t size_directory = 0;
	int i;
	while(0 != (size_directory = dir_read(buf,32))){
		for(i = 0; i < size_directory; i++){ 
			printf("%c", buf[i]);
		}
		printf("\n");
	}*/
	
	//this tests read_dentry_by_index
/*	int i;
	uint32_t index = 1;
	//dentry_t* test_dentry = &(bootblock_addr->entries[0]);
	int8_t test_dentry[64]; //can't access struct like this
	if(read_dentry_by_index(index, test_dentry) == 0){
		for(i = 0; i < 32; i++){
			printf("%c", test_dentry[i]);
		}
		printf("\n");
		//for(i = 32; i < 36;i++){
			printf("%d", test_dentry[32]); //filetype
	//	}
		printf("\n");
		//for(i = 36; i < 40;i++){
			printf("%d", test_dentry[36]);//inode
	//	}
		printf("\n");
	}*/
	
	//this tests read_dentry_by_name
	/*int i;
	uint32_t index = 1;
	int8_t test_dentry[64]; //can't access struct like this
	if(read_dentry_by_name((uint8_t*)"frame1.txt", test_dentry) == 0){
		for(i = 0; i < 32; i++){
			printf("%c", test_dentry[i]);
		}
		printf("\n");
		//for(i = 32; i < 36;i++){
			printf("%d", test_dentry[32]); //filetype
	//	}
		printf("\n");
		//for(i = 36; i < 40;i++){
			printf("%d", test_dentry[36]);//inode
	//	}
		printf("\n");
	}*/
	
	//this tests file_read size of directory
	/*int sizebuf = 150;
	int8_t buf[sizebuf];
	int z;
	for(z = 0; z <sizebuf; z++){
		buf[z] = 0;
	}
	int32_t size_directory = 0;
	int i;
	size_directory = file_read(buf, sizebuf);
	for(i = 0; i < size_directory; i++){ 
		printf("%c", buf[i]);
	}
	printf("\n%d", size_directory);*/
	
	
	//this one further tests file_read, multiple calls
	/*int sizebuf = 10000;
	int sizebuf2 = 7000;
	int8_t buf[sizebuf + sizebuf2];
	int z;
	for(z = 0; z <(sizebuf + sizebuf2); z++){
		buf[z] = 0;
	}
	int32_t size_directory = 0;
	int i;
	size_directory = file_read(buf, sizebuf);
	for(i = 0; i < sizebuf; i++){ 
		//printf("%c", buf[i]);
	}
	printf("\n%d", size_directory);
	size_directory = file_read(buf, sizebuf2);
	for(i = sizebuf; i < (sizebuf+sizebuf2); i++){ 
		//printf("%c", buf[i]);
	}
	printf("\n%d", size_directory);*/
	/*size_directory = file_read(buf, sizebuf2);
	for(i = sizebuf; i < (sizebuf+sizebuf2); i++){ 
		printf("%c", buf[i]);
	}
	printf("\n%d", size_directory);*/

	//enable_irq(1); // keyboard
	

	
	/* Initialize devices, memory, filesystem, enable device interrupts on the
	 * PIC, any other initialization stuff... */

	/* Enable interrupts */
	/* Do not enable the following until after you have set up your
	 * IDT correctly otherwise QEMU will triple fault and simple close
	 * without showing you any output */
	//printf("Enabling Interrupts\n");
	
	//open_rtc(NULL);
	sti();
			//initialize rtc (OPEN RTC)
	//printf("\n");
	/*int i;
	sti();
	write_rtc(2);
	for(i=0; i<15 ; i++)
		read_rtc();		//test that rtc is being read
	write_rtc(8);
	for(i=0; i<15; i++)
		read_rtc();
	write_rtc(16); //512
	for(i=0; i<15; i++)
		read_rtc();
	close_rtc();*/
	/* Execute the first program (`shell') ... */
	
	/* Spin (nicely, so we don't chew up cycles) */
	asm volatile(".1: hlt; jmp .1;");
}

