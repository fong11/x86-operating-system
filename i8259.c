 /* i8259.c - Functions to interact with the 8259 interrupt controller
 * vim:ts=4 noexpandtab
 */

#include "i8259.h"
#include "lib.h"

/* Interrupt masks to determine which interrupts
 * are enabled and disabled */
uint8_t master_mask = 0xFF; /* IRQs 0-7 */
uint8_t slave_mask = 0xFF; /* IRQs 8-15 */

/* Initialize the 8259 PIC */
void
i8259_init(void)
{
	outb(0xFF, MASTER_8259_PORT2);	// mask master
	outb(0xFF, SLAVE_8259_PORT2);	// mask slave 
	
	
	outb(ICW1, MASTER_8259_PORT);	
	outb(ICW2_MASTER, MASTER_8259_PORT2);	
	outb(ICW3_MASTER, MASTER_8259_PORT2);	
									

	outb(ICW4, MASTER_8259_PORT2);

	outb(ICW1, SLAVE_8259_PORT);	
	outb(ICW2_SLAVE, SLAVE_8259_PORT2);	
	outb(ICW3_SLAVE, SLAVE_8259_PORT2);	 
											
	outb(ICW4, SLAVE_8259_PORT2);  

	outb(master_mask, MASTER_8259_PORT2); /* restore mask */
	outb(slave_mask, SLAVE_8259_PORT2);	  /* restore mask */
}

/* Enable (unmask) the specified IRQ 
might have to change this and disable_irq (irq_num & 8) to (irq_num >= 8)
works because we are only using irq8 with the rtc*/
void
enable_irq(uint32_t irq_num)
{
	unsigned int mask = ~(1 << irq_num); // only irq_num is set active low,
										//everything else masked
	//update masks
	master_mask &= mask;
	slave_mask &= mask >> 8;
	if (irq_num >= 8)
		outb(slave_mask, SLAVE_8259_PORT2);
	else
		outb(master_mask, MASTER_8259_PORT2);
		
}

/* Disable (mask) the specified IRQ */
void
disable_irq(uint32_t irq_num)
{
	unsigned int mask = 1 << irq_num;

	master_mask |= mask;
	slave_mask |= mask >> 8;
	if (irq_num >= 8)
		outb(slave_mask, SLAVE_8259_PORT2);
	else
		outb(master_mask, MASTER_8259_PORT2);
}

/* Send end-of-interrupt signal for the specified IRQ */
void
send_eoi(uint32_t irq_num)
{
	//eoi is 0x60
	if(irq_num >= 8){

		outb(EOI | (irq_num - 8), SLAVE_8259_PORT); //fixed bug here
		outb(EOI | 0x02, MASTER_8259_PORT); // or by 0x02 because of irq2
	}
	else{
		outb(EOI | irq_num, MASTER_8259_PORT);
	}
}

