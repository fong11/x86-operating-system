#include "scheduler.h"
#include "syscalls.h"
#include "lib.h"
#include "multiboot.h"
#include "types.h"
#include "keyboard.h"
#include "x86_desc.h"
#include "rtc.h"
#include "i8259.h"
#include "filesystem.h"
#include "paging.h"

uint32_t p_eax, p_ebx, p_ecx, p_edx, p_edi, p_esi, u_esp, p_eip, us;
int current_run_term =0;
/*
 * pit_handler
 *	 DESCRIPTION:  Catches the PIT interrupt and calls the scheduler
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *   SIDE EFFECTS: saves registers into the pcb of the current process
 */
void pit_handler(){

	/*if(num_processes > 0){
	//get_process
	pcb_t* curr_proc;
	curr_proc = get_process();
	
	//save registers into that curr_proc
	curr_proc->eax = p_eax;
	curr_proc->ebx = p_ebx;
	curr_proc->ecx = p_ecx;
	curr_proc->edx = p_edx;
	curr_proc->edi = p_edi;
	curr_proc->esi = p_esi;
	curr_proc->term_eip = p_eip;
	curr_proc->term_ebp = u_esp;
	curr_proc->cs = us;
	
	
	terminal_screens[curr_proc->parent_term].pcb = curr_proc;
	

	asm volatile("		\n"
	"movl %%eax, %0		\n"
	"movl %%ebx, %1  	\n"
	"movl %%ecx, %2		\n"
	"movl %%edx, %3		\n"
	"movl %%edi, %4		\n"
	"movl %%esi, %5		\n"
	:"=r"(curr_proc->eax), "=r"(curr_proc->ebx), "=r"(curr_proc->ecx), "=r"(curr_proc->edx), "=r"(curr_proc->edi), "=r"(curr_proc->esi)
	:);

	
		
	//scheduler();
	}*/
	send_eoi(0);
	sti();
}
/*
 * scheduler
 *	 DESCRIPTION:  Finds the process we want to switch through by the 
					Round robin algorithm. Restores Registers, and then 
					pushes the correct information on the stack and then
					calls iret
	 INPUTS: none
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *   SIDE EFFECTS: runs the processes in round robin order
 */
void scheduler(){
		//determine terminal with active process
		pcb_t* curr_proc;
		current_run_term = 0;
		//current_run_term++;
		current_run_term %= 3;
		
		if(terminal_screens[current_run_term].pcb != NULL){
			curr_proc = terminal_screens[current_run_term].pcb;
		}else{
			current_run_term--;
			current_run_term %= 3;
			return;
		}
			

		
		int proc_num = curr_proc->proc_num;
		
		//update page table

		map_virtual_mem(0x800000 + (proc_num)*0x400000);
	
		
			
		//esp0 and ss0
		tss.ss0 = KERNEL_DS;
		tss.esp0 = 0x800000 - (proc_num)*2*4096;
	 
		//restore all of the registers
		asm volatile("		\n"
		"movl %0, %%eax		\n"
		"movl %1, %%ebx		\n"
		"movl %2, %%ecx		\n"
		"movl %3, %%edx		\n"
		"movl %4, %%edi		\n"
		"movl %5, %%esi		\n"
		:
		:"r"(curr_proc->eax), "r"(curr_proc->ebx), "r"(curr_proc->ecx), "r"(curr_proc->edx), "r"(curr_proc->edi), "r"(curr_proc->esi));
		
		//push correct stuff onto stack and call iret

		asm volatile("pushl %0"
						:
						:"r"(USER_DS)
						);
		asm volatile("pushl %0;"
						:
						:"r"(curr_proc->term_ebp)
						);
		asm volatile("pushl %0;"
						:
						:"r"(curr_proc->flags)
						);
		asm volatile("pushl %0;"
						:
						:"r"(curr_proc->cs)
						);								
		asm volatile("pushl %0; "
						:
						:"r"(curr_proc->term_eip)
						);	
		send_eoi(0);
		sti();
		asm volatile("iret"
						:
						:
						);	
		
	
}


