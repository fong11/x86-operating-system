#ifndef _ASSEMBLY_HANDLERS_H
#define _ASSEMBLY_HANDLERS_H

//these are all the handlers that are called by this
extern void divide_exception_handle();
extern void debug_exception_handle();
extern void breakpoint_exception_handle();
extern void overflow_exception_handle();
extern void boundrange_exception_handle();
extern void invalid_op_exception_handle();
extern void no_device_exception_handle();
extern void doublefault_exception_handle();
extern void coprocessor_seg_overrun_handle();
extern void invalidTSS_exception_handle();
extern void no_segment_handle();
extern void stackfault_exception_handle();
extern void generalprot_exception_handle();
extern void pagefault_exception_handle();
extern void floatingpoint_error_handle();
extern void alignmentcheck_exception_handle();
extern void machinecheck_exception_handle();
extern void simd_floatingpoint_exception_handle();
extern void pit_handle();
extern void keyboard_handle();
extern void rtc_handle();
extern void system_call_handle();

//global variables so that we can context switch
extern uint32_t p_eax;
extern uint32_t p_ebx;
extern uint32_t p_ecx;
extern uint32_t p_edx;
extern uint32_t p_edi;
extern uint32_t p_esi;
extern uint32_t u_esp;
extern uint32_t p_eip;
extern uint32_t us; 
#endif


