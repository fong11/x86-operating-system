
#include "mp3_handler.h"
#include "keyboard.h"
#include "i8259.h"
#include "lib.h"
#include "rtc.h"
/*
 * divide_exception_handler
 *	 DESCRIPTION: prints divide by zero
	 INPUTS: none.
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *   SIDE EFFECTS: none
 */
void divide_exception_handler()
{
	printf("EXCEPTION: Divide by Zero \n");
	while(1);
}

/*
 * debug_exception_handler
 *	 DESCRIPTION: prints exception: debug
	 INPUTS: none.
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *   SIDE EFFECTS: none
 */
void debug_exception_handler()
{	
	printf("\nEXCEPTION: DEBUG\n");	
	while(1);
}

/*
 * nmi_exception_handler
 *	 DESCRIPTION: prints exception: nmi
	 INPUTS: none.
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *   SIDE EFFECTS: none
 */
void nmi_interrupt_handler()
{
	printf("\nEXCEPTION : NMI\n");
	while(1);
}

/*
 * breakpoint_exception_handler
 *	 DESCRIPTION: prints exception: got breakpoint
	 INPUTS: none.
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *   SIDE EFFECTS: none
 */
void breakpoint_exception_handler()
{
	printf("\n EXCEPTION: GOT BREAKPOINT\n");
	while(1);
}

/*
 * overflow_exception_handler
 *	 DESCRIPTION: prints exception: overflow
	 INPUTS: none.
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *   SIDE EFFECTS: none
 */
void overflow_exception_handler()
{
	printf("\nException: Overflow \n");
	while(1);
}

/*
 * boundrange_exception_handler
 *	 DESCRIPTION: prints exception: bounds
	 INPUTS: none.
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *   SIDE EFFECTS: none
 */
void boundrange_exception_handler()
{
	printf("\nException: Bounds \n");	
	while(1);
}

/*
 * invalid_op_exception_handler
 *	 DESCRIPTION: prints exception: invalid op handle
	 INPUTS: none.
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *   SIDE EFFECTS: none
 */
void invalid_op_exception_handler()
{
	printf("\nException: Invalid Op Handle \n");
	while(1);
}

/*
 * no_device_exception_handler
 *	 DESCRIPTION: prints exception: device not found
	 INPUTS: none.
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *   SIDE EFFECTS: none
 */
void no_device_exception_handler()
{
	printf("\nException: Device not found \n");
	while(1);
}

/*
 * doublefault_exception_handler
 *	 DESCRIPTION: prints exception: double fault
	 INPUTS: none.
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *   SIDE EFFECTS: none
 */
void doublefault_exception_handler()
{
	printf("\nException: Double Fault\n");
	while(1);
}

/*
 * coprocessor_seg_overrun_handler
 *	 DESCRIPTION: prints exception: Coprocessor Segment Overrun
	 INPUTS: none.
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *   SIDE EFFECTS: none
 */
void coprocessor_seg_overrun_handler()
{
	printf("\nException: Coprocessor Segment Overrun \n");
	while(1);
}

/*
 * invalidTSS_exception_handler
 *	 DESCRIPTION: prints exception: invalid TSS hardware error code
	 INPUTS: none.
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *   SIDE EFFECTS: none
 */
void invalidTSS_exception_handler()
{
	printf("\nException: Invalid TSS\n Hardware Error Code\n");
	while(1);
}

/*
 * no_segment_handler
 *	 DESCRIPTION: prints exception: no Segment
	 INPUTS: none.
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *   SIDE EFFECTS: none
 */
void no_segment_handler()
{
	printf("\nException: No Segment\n Hardware Error Code\n");
	while(1);
}

/*
 * stackfault_exception_handler
 *	 DESCRIPTION: prints exception: stack segment
	 INPUTS: none.
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *   SIDE EFFECTS: none
 */
void stackfault_exception_handler()
{
	printf("\nException: Stack Segment\n Hardware Error Code\n");
	while(1);
}

/*
 * generalprot_exception_handler
 *	 DESCRIPTION: prints exception: general protection error
	 INPUTS: none.
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *   SIDE EFFECTS: none
 */
void generalprot_exception_handler()
{ 
	printf("Exception:General Protection Error \n");
	while(1);
}

/*
 * pagefault_exception_handler
 *	 DESCRIPTION: prints exception: page fault
	 INPUTS: none.
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *   SIDE EFFECTS: none
 */
void pagefault_exception_handler()
{
	printf("\nException: Page Fault\n");
	while(1);
}

/*
 * floatingpoint_error_handler
 *	 DESCRIPTION: prints exception: floating point
	 INPUTS: none.
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *   SIDE EFFECTS: none
 */
void floatingpoint_error_handler()
{
	printf("\nException: Floating point\n ");
	while(1);
}

/*
 * alignmentcheck_exception_handler
 *	 DESCRIPTION: prints exception: aligment check
	 INPUTS: none.
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *   SIDE EFFECTS: none
 */
void alignmentcheck_exception_handler()
{
	printf("\n Exception: Alignment Check\n ");
	while(1);
}

/*
 * machinecheck_exception_handler
 *	 DESCRIPTION: prints exception: machine check
	 INPUTS: none.
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *   SIDE EFFECTS: none
 */
void machinecheck_exception_handler()
{
	printf("\nException: Machine Check \n ");
	while(1);
	
}

/*
 * simd_floatingpoint_exception_handler
 *	 DESCRIPTION: prints exception: floating point handler
	 INPUTS: none.
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *   SIDE EFFECTS: none
 */
void simd_floatingpoint_exception_handler()
{
	printf("\nException: floating point handler\n ");
	while(1);
}

/*
 * system_call_handler
 *	 DESCRIPTION: prints exception: system call
	 INPUTS: none.
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *   SIDE EFFECTS: none
 */
void system_call_handler()
{
	printf("\nException: System Call\n ");
	while(1);
}
/*
 * rtc_handler
 *	 DESCRIPTION: sets flags and ports
	 INPUTS: none.
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *   SIDE EFFECTS: none
 */
void rtc_handler(){
	//test_interrupts();



	outb(0x8C, 0x70);
	char temp=inb(0x71);
	temp = temp; //to get rid of the warnings
	
	send_eoi(8);
	rtc_flag = 1;
	
}

