#include "idt.h"
#include "multiboot.h"
#include "x86_desc.h"
#include "lib.h"
#include "i8259.h"
#include "debug.h"
#include "assembly_handlers.h"
#include "mp3_handler.h"
#include "keyboard.h"
#include "paging.h"
#include "rtc.h"

/*
 * open_rtc
 *	 DESCRIPTION: Disables NMI for initialization and then writes bits to register B to enable
				  periodic interrupts.
	 INPUTS: none.
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *   SIDE EFFECTS: none

 *   information taken from OSDEV.ORG
 */
//static int test = 0;
uint32_t open_rtc(const uint8_t* filename){
	enable_irq(8); // rtc
	// Initialize RTC and set the frequency (0x71 is the read/write port)
    // 4 Hertz is the default frequency
    outb(REGA, REGISTER);                       //disables non-maskable interrupts
    outb(DATAPORT, RW);                     	//putting the frequency information into the data port
    // Move register port to register B
    outb(REGB, REGISTER);                       //0x70 is the register port and setting it to B
    char testchar = inb(RW);      				//reading in the char from the data port
    testchar |= PIE;                  	     //get the PIE bit
    outb(REGB, REGISTER);                       //register B to register port
    outb(testchar, RW);		
	

	return 0;
}

/*
 * close_rtc
 *	 DESCRIPTION: Enables NMI for initialization and then writes bits to register B to enable
				  periodic interrupts.
	 INPUTS: none.
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *   SIDE EFFECTS: none
 *   information taken from OSDEV.ORG
 */

uint32_t close_rtc(int32_t fd){
	cli();									//writing to register port should not be allowed when conducting the following code
	// Initialize RTC and set the frequency (0x71 is the read/write port)
    outb(REGA, REGISTER);                       //disables non-maskable interrupts
    outb(DATAPORT, RW);                       //putting the frequency information into the data port
    // Move register port to register B
    outb(REGB, REGISTER);                       //0x70 is the register port and setting it to B
    char testchar = inb(RW);      		//reading in the char from the data port
    testchar &= BIT_RESET;                       //Reset the bit for close
    outb(REGB, REGISTER);                       //register B to register port
    outb(testchar, RW);		
    sti();
	return 0;
}


/*
 * read_rtc
 *	 DESCRIPTION: Wait for an interrupt
	 INPUTS: none
 *   OUTPUTS: flag that is set when waiting for interrupt
 *   RETURN VALUE: value of flag (0 or 1)
 *   SIDE EFFECTS: none
 *   information taken from OSDEV.ORG
 */
uint32_t read_rtc(int32_t fd, void* buf, int32_t nbytes){
	rtc_flag = 0;								//flag that is set when an rtc interrupt is read, it will change to 1 and it will print 
												//rtc interrupt has occurred. 
	while(rtc_flag==0){
	}
	//printf("RTC interrupt has occurred.");
	//printf("\n");
	//test++;
	//printf("%d", test);
	//printf("\n");
	return 0;
}

/*
 * write_rtc
 *	 DESCRIPTION: Disables NMI for initialization and then returns the value that is read at the 
				  given register.
	 INPUTS: register which needs to be read and value that needs to be written
 *   OUTPUTS: none
 *   RETURN VALUE: 0
 *   SIDE EFFECTS: none
 *   information taken from OSDEV.ORG
 */
uint32_t write_rtc(int32_t fd, const void* buf, int32_t nbytes){
	cli();
	int frequency = *(int*)buf;

	switch(frequency)
	{
		case 2:
   			outb(REGA, REGISTER);                       //disables non-maskable interrupts
    		outb(0xAF, RW);                       //putting the frequency information into the data port	
			break;
		case 4:
   			outb(REGA, REGISTER);                       //disables non-maskable interrupts
    		outb(0xAE, RW);                       //putting the frequency information into the data port	
			break;		
		case 8:
   			outb(REGA, REGISTER);                       //disables non-maskable interrupts
    		outb(0xAD, RW);                       //putting the frequency information into the data port	
			break;
		case 16:
   			outb(REGA, REGISTER);                       //disables non-maskable interrupts
    		outb(0xAC, RW);                       //putting the frequency information into the data port	
			break;
		case 32:
   			outb(REGA, REGISTER);                       //disables non-maskable interrupts
    		outb(0xAB, RW);                       //putting the frequency information into the data port	
			break;
		case 64:
   			outb(REGA, REGISTER);                       //disables non-maskable interrupts
    		outb(0xAA, RW);                       //putting the frequency information into the data port	
			break;
		case 128:
   			outb(REGA, REGISTER);                       //disables non-maskable interrupts
    		outb(0xA9, RW);                       //putting the frequency information into the data port	
			break;
		case 256:
   			outb(REGA, REGISTER);                       //disables non-maskable interrupts
    		outb(0xA8, RW);                       //putting the frequency information into the data port	
			break;
		case 512:
   			outb(REGA, REGISTER);                       //disables non-maskable interrupts
    		outb(0xA7, RW);                       //putting the frequency information into the data port	
			break;
		case 1024:
   			outb(REGA, REGISTER);                       //disables non-maskable interrupts
    		outb(0xA6, RW);                       //putting the frequency information into the data port	
			break;
		case 2048:
   			outb(REGA, REGISTER);                       //disables non-maskable interrupts
    		outb(0xA5, RW);                       //putting the frequency information into the data port	
			break;
		default: 
			return -1;

	}

	sti();
	return 0;

}

