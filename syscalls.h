#ifndef _SYSCALLS_H
#define _SYSCALLS_H

#include "lib.h"
#include "multiboot.h"
#include "types.h"
#include "x86_desc.h"
#include "rtc.h"
#include "i8259.h"
#include "filesystem.h"
#include "paging.h"

//WHEN WE CHANGE THIS CHANGE THE BEGINNING OF HALT, NUMPROCESSES == 3
#define MAX_NUM_OF_PROCESSES 6
#define PROGRAM_IMAGE_ADDR 0x08048000


int32_t halt(uint8_t status);
int32_t execute(const uint8_t* command);
int32_t open(const uint8_t* filename);

int get_proc_num();
extern int num_processes;
extern int process_array[MAX_NUM_OF_PROCESSES];

typedef struct file_operations{
       uint32_t (*read)(int32_t , void* , int32_t);
       uint32_t (*write)(int32_t , const void* , int32_t);
       uint32_t (*open)(const uint8_t*);
       uint32_t (*close)(int32_t);
    }file_operations_t;

file_operations_t file_op[5];
extern void init_ops();
typedef struct file{

	file_operations_t *file_ops;	//Jump table of file ops
	uint32_t inode ;				//inode
	uint32_t file_pos;			//Current position in file (bytes)
	uint32_t flags;	
	//uint32_t f_count;			//Number of times file is accessed
	
}file_t;

//Process Control Block
typedef struct pcb{
	file_t file_array[8];	//File array
	uint8_t args[150];
	struct pcb* parent_pcb;
	int parent_term;
	int proc_num;			//Process id
	
	//Registers of program state
	uint32_t eax;	
	uint32_t esp;
	uint32_t ebp;
	uint32_t term_ebp;
	uint32_t edi;
	uint32_t esi;
	uint32_t edx;
	uint32_t ecx;
	uint32_t ebx;
	uint32_t term_eip;
	uint32_t ds;
	uint32_t cs;
	uint32_t flags;
	uint32_t eip;
}pcb_t;
extern pcb_t processes[MAX_NUM_OF_PROCESSES];
pcb_t pcbarray[2];
pcb_t* get_process(void);

#endif
