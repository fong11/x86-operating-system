#ifndef _KEYBOARD_H
#define _KEYBOARD_H

#include "types.h"
#include "lib.h"
#include "x86_desc.h"
#include "syscalls.h"
#define NEW_ATTRIB 0x07
#define SCREEN_SIZE 2000
#define BACKSPACE 0x0E
#define ENTER 0x1C
#define BUFFER_MAX 128
#define L_SHIFT 0x2a
#define R_SHIFT 0x36
#define CTRL_KEY 0x1D
#define ALT_KEY 0x38
#define F1_KEY 0x3B
#define F2_KEY 0x3C
#define F3_KEY 0x3D

extern uint32_t terminal_open();
extern uint32_t terminal_read(int32_t fd, void* buf, int32_t nbytes);
extern uint32_t terminal_write(int32_t fd, const void* buf, int32_t nbytes);
extern uint32_t terminal_close();
void backspace_fn();
void print_chr_on_screen(char c);
void initialise_terminals();
void process_switch(int term);


file_operations_t fops_term;


typedef struct terminal_screen{
	//unsigned char screen_buf[SCREEN_SIZE];
	unsigned char key_buf[SCREEN_SIZE];
	int key_count;
	int screen_x;
	int screen_y;
	int root_shell;
	pcb_t* pcb;

}terminal_screen_t;
void clear_screen(int term);
void switch_terminals(int term_num);

void term_putc(uint8_t c, int term);
extern int curr_terminal;
extern int term_process_info[3];
extern terminal_screen_t terminal_screens[3];
//extern char* video_mem = (char *)VIDEO;
//extern void clear(void);
#endif
