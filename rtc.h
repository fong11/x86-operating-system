#ifndef _RTC_H
#define _RTC_H
#include "lib.h"
#define REGISTER 0x70
#define RW 0x71
#define REGA 0x8A
#define REGB 0x8B
#define DATAPORT 0xAE
#define PIE 0x40
#define BIT_RESET 0xB4

extern uint32_t open_rtc(const uint8_t* filename);
extern uint32_t close_rtc(int32_t fd);
extern uint32_t read_rtc(int32_t fd, void* buf, int32_t nbytes);
extern uint32_t write_rtc(int32_t fd, const void* buf, int32_t nbytes);




#endif
