#ifndef _IDT_TABLE_H 
#define _IDT_TABLE_H 
  
void set_intr_gate(int vec, void* handler); 
void set_trap_gate(int vec, void* handler); 
void set_task_gate(int vec, void* handler); 
  
#endif /* _IDT_TABLE_H */ 
